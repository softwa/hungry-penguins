(** Penguin module *)

(** represents a penguin, i.e. a position and a player *)
type penguin = Board.pos * Player.player

(** used to initialize various things *)
let empty = Board.empty_pos, Player.empty
