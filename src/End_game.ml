open Game_state
open Penguin
open Player
open Move
open Board

exception Pascuila
exception Pasparla

(**Function for computing end game value*)

(** These function are modified version of the function from the Computer module*)




let num_penguin situation p =
	let tab = snd(situation) in
	let k = ref 0 in
	for i = 0 to Array.length(tab)-1 do
		if fst(tab.(i)) = p then k:=i;
	done;
	!k




(**Return distance of best case to go in the direction d for the penguin cuila, and the value of said case*)
let end_game_choose_distance situation position_du_manchot d fct_max=
	(* on peurt utiliser n'importe quel fonction de calcul de l'optimal, du moment qu'elle a le même type que max_fish*)
	let l = check_mvt (fst(situation)) (snd(situation)) position_du_manchot d in
	let np = num_penguin situation position_du_manchot in
	if (l=[]) then raise Pasparla;
	let distance = ref 0 in
	let max = ref 0 in
	let best_dist = ref 0 in
	let aux p = 
		incr distance;
		let new_fish = (fst(situation)).(fst(p)).(snd(p)) in
		let new_sit = apply_move np (!distance) d (fst(situation)) (snd(situation)) in
		let value = fct_max new_sit p in
		if (value+ new_fish) > (!max) then (max := value+ new_fish ; best_dist := !distance);
	in
	List.iter aux l;
	if (!best_dist =0) then raise Pasparla;
	(!best_dist, !max)





(**Select the best direction to go towards*)
let end_game_choose_direction situation position_du_manchot fct_max=
	let d = ref N in
	let distance = ref (-1) in
	let max = ref (-1) in
	let aux direction =
		try 
			let (a,b) =(end_game_choose_distance situation position_du_manchot direction fct_max) in
			if (b> !max) then (max:=b ; distance := a; d:=direction)
		with
		| Pasparla-> ()	in
	List.iter aux all_directions;
	if (!max = -1) then raise Pascuila; (*this should not happen*)
	(!d, !distance, !max)


let can_move situation (p:pos) = 
	let aux direction = 
		if (check_mvt (fst(situation)) (snd(situation)) p direction = [] ) then false else true
	in
	let l = List.map aux all_directions in
	(List.mem true l)


(** When applied to a penguin which is isloated, compute the maximum score it can do *)
let rec end_game situation position_du_manchot =
	let pas_fini = can_move situation position_du_manchot in
	if pas_fini then (let (a,b,c) = end_game_choose_direction situation position_du_manchot end_game in c)
		else 0









