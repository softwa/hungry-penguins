open Move


(**Functions to test if a penguin is alone on a part of the ice*)

exception Deja_qn of int * int

(*Cette fonction va fonctionner en propageant le numéro du manchot aux cases alentour, si l'on rencontre un autre numéro c'est que les manchots sont sur le même morceau de banquise*)
(**This function return an array of same size of the one containing the penguin, which contains true if and only of the penguin is alone on a piece of the board*)
let end_test (situation: Game_state.game_state) =
	let g = fst(situation) and peng_tab = snd(situation) in
	let alone = Array.make (Array.length peng_tab) true in
	let propagation_grid = Array.make_matrix 4 15 (-1) in
	for i  = 0 to 3 do
		for j = 0 to 14 do
			if g.(i).(j) = 0 then propagation_grid.(i).(j) <- -2;
		done;
	done;

	(* chaque case de propagation_grid vaut -2 si il n'y a pas de banquise, -1 si il y en a et que personne ne s'y est propagé, ou le numéro du manchot qui s'y est propagé*)

	let rec propage p (num_peng : int) : unit =
		(* fonction qui propage le numéro du manchot*)
		let aux d = 
			let p2 = case_suivante p d in
			(* on teste d'abord si on est dans les bornes*)
			if (fst(p2) >= 0) && (fst(p2)<4) && (snd(p2)>=0) && (snd(p2)<15) then
				(*premier cas: il y a quelqu'un sur la case*)
				(if (Game_state.get_player_on p2 peng_tab <> None) then
					(let occupant = End_game.num_penguin situation p2 in
					if (occupant<>num_peng) then raise (Deja_qn (num_peng, occupant)));
				if (propagation_grid.(fst(p2)).(snd(p2)) >= 0) then
				(* deuxième cas, il y a déjà quelqu'un qui s'est propagé.*)
					(let a = propagation_grid.(fst(p2)).(snd(p2)) in
					if (a<>num_peng) then raise (Deja_qn (num_peng,a)));
				(*troisième cas, personne ne s'est propagé sur la case*)
				if (propagation_grid.(fst(p2)).(snd(p2)) = -1) then
					(propagation_grid.(fst(p2)).(snd(p2))<- num_peng;
					propage p2 num_peng );
			
				);
		in
		List.iter aux all_directions
	in
	for i = 0 to (Array.length peng_tab)-1 do
		try
			propage (fst(peng_tab.(i))) i
		with
		|Deja_qn (a,b) -> alone.(a) <- false; alone.(b) <- false;
	done;
	alone
