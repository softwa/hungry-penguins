(** Client module *)

open Game_state

(** used to know if the client wants a text or a graphic display *)
type render_type = Text | Graphic
(** represents a request to move, which will be sent to the server *)
type request = Move of int * Move.dir * int

(** debug purposes *)
let log s = print_string s

(** return a request as a string *)
let request_to_string = function
  | Move (penguin, dir, blocks) -> "move " ^ (string_of_int penguin) ^ " " ^ (string_of_int @@ Move.int_of_dir @@ dir) ^ " " ^ (string_of_int blocks)

(** initialisation function *)
let init_client () =

  (* TODO: parse this from args *)
  let port = 10234 in
  let machine = Unix.gethostname () in

  log ("trying to connect to server " ^ machine ^ " on port " ^ (string_of_int port) ^ "...\n");

  Network.connect_to_server machine port;

  log "connected, waiting for the game to start...\n";

  Text (* TODO: parse args to allow Graphic *)

(** wait until the server sends a msg *)
let rec wait_msg () =
  try Network.get_server_msg ()
  with Network.No_Msg -> wait_msg ()

(** display a game state  *)
let render_game_state g = function
  | Text -> print_string (Game_state.to_string g)
  | Graphic -> failwith "TODO: render_game_state Graphic"

(* TODO: remove this when 4.05 works everywhere... *)
(** temporary function, native in OCaml 4.05 *)
let read_int_opt () =
  try Some (read_int ()) with Failure _ -> None

(** ask the player to select a move *)
let wait_text_input g penguinsPerPlayer myNum =

  let _, penguins = g in

  let myPenguinsPos = Game_state.playerspenguins (Player.of_num myNum) penguins in
  let myPenguinsPos = List.map (fun (pos, _) -> pos) myPenguinsPos in
  let myPenguinsString = List.mapi (fun i (posX, posY) ->
      let start = if i = 0 then "" else " | " in
      (start ^ "(" ^ (string_of_int posX) ^ "," ^ (string_of_int posY) ^ "):" ^ (string_of_int i))
    ) myPenguinsPos
  in
  let myPenguinsString = List.fold_left (fun acc el -> acc ^ el) "" myPenguinsString in

  let rec choose_penguin () =
    print_string ("choose a penguin number (" ^ myPenguinsString ^ "):\n");
    match read_int_opt () with
    | None -> choose_penguin ()
    | Some n ->
      if n < 0 || n > penguinsPerPlayer then choose_penguin () else n
  in

  let penguin = choose_penguin () in

  let rec choose_direction () =
    print_string "choose a direction (N:0 | NE:1 | SE:2 | S:3 | SW:4 | NW:5):\n";
    match read_int_opt () with
    | None -> choose_direction ()
    | Some n ->
      try Move.dir_of_int n with Move.InvalidMoveNumber -> choose_direction ()
  in

  let dir = choose_direction () in

  let rec choose_nb_of_blocks () =
    print_string "choose a number of blocks to cross:\n";
    match read_int_opt () with
    | None -> choose_nb_of_blocks ()
    | Some n ->
      (* TODO: get the size of the plate and check this more precisely *)
      if n < 0 then choose_nb_of_blocks () else n
  in

  let blocks = choose_nb_of_blocks () in

  penguin, dir, blocks

(** wait player input *)
let wait_input g penguinsPerPlayer myNum = function
  | Text -> wait_text_input g penguinsPerPlayer myNum
  | Graphic -> failwith "TODO: wait_input Graphic"

(** main function *)
let _ =

  let me = init_client () in

  let nbOfPlayers, myNum = wait_msg () in

  let rec mainloop () =
    let gameState = wait_msg () in
    render_game_state gameState me;
    let m1, m2, m3 = wait_input gameState (Game_state.penguins_per_player nbOfPlayers) myNum me in
    Network.send_msg_to_server (request_to_string (Move (m1, m2, m3)));
    mainloop ()
  in

  mainloop ();

  Network.disconnect_from_server ();

  ()
