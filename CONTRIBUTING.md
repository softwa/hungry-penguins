# Contributing

## How to create a merge request

### First time

[Fork] the main (called `upstream`) repository.

You now have your own copy of the repository, available at `https://gitlab.com/user/hungry-penguins` where `user` is your user name on GitLab.

Clone your fork on your computer (replace `user` by your user name):

```sh
git clone https://gitlab.com/user/hungry-penguins.git
```

Add `upstream` as a remote called `upstream`:

```sh
git remote add upstream https://gitlab.com/softwa/hungry-penguins.git
```

### On each merge request

Fetch new stuff from `upstream`:

```sh
git fetch upstream
```

Make sure you're on your master branch:

```sh
git checkout master
```

Update your master so it's in sync with `upstream/master`:

```sh
git rebase upsteam/master
```

Create a new branch for your MR (replace `amazingfeature` with something... better ?):

```sh
git branch amazingfeature
```

Switch to your new branch:

```sh
git checkout amazingfeature
```

Write code, create commits etc.

Push your commits to a new branch on your fork:

```sh
git push --set-upstream origin amazingfeature
```

Create your merge request using the correct button on GitLab.

Wait for the CI to be completed, if everything is going well, it's ready to merge. Otherwise, you can update the branch `amazingfeature` on your fork, the MR will be updated automatically.

## Building and writing documentation

The documentation can be build using:

```sh
scripts/build_doc.sh
```

You can now see it by opening `hungry_penguins_common_api.docir/index.html` or using:

```sh
scripts/open_doc.sh
```

To write documentation, see the [syntax of documentation comments].

## Writing and running tests

To run tests, you need `shellcheck` on your system. Then:

```sh
scripts/test.sh
```

New tests for the OCaml code should be added in `test/test.ml`. See the [alcotest documentation] to learn how to write tests.

[Fork]: https://gitlab.com/softwa/hungry-penguins/forks/new
[syntax of documentation comments]: http://caml.inria.fr/pub/docs/manual-ocaml/ocamldoc.html#sec333
[alcotest documentation]: (https://mirage.github.io/alcotest/alcotest/Alcotest/index.html)
